#!/usr/bin/python
# DESCRIPTION:
# Script fit sky background with a polynomial of n degree. A mask is required.
# MINIMAL USAGE: python determine_sky.py [input_image] [input_mask]

import numpy as np
from astropy.io import fits as pyfits
from astropy.modeling import models, fitting
import warnings
warnings.filterwarnings("ignore")
from astropy.stats import sigma_clipped_stats
import argparse

def convert_segm_to_boolean(mask):
    return np.ma.make_mask(mask, shrink=False)

def split_xyz(data, mask=None, arrays=False):
    # Initialize lists to contain the x, y and z values
    x_values = []
    y_values = []
    z_values = []

    # Loop over all x and y values
    for x in range(data.shape[1]):
        for y in range(data.shape[0]):

            # If no mask is specified or the pixel is not masked, add the coordinates and value to the appropriate lists
            if mask is None or not mask[y,x]:
                x_values.append(x)
                y_values.append(y)
                z_values.append(data[y,x])

    if arrays: return np.array(x_values), np.array(y_values), np.array(z_values)
    else: return x_values, y_values, z_values

def evaluate_model(model, x_min, x_max, y_min, y_max, x_delta=1, y_delta=1):
    # Create x and y meshgrid for evaluating the model
    y_plotvalues, x_plotvalues = np.mgrid[y_min:y_max:y_delta, x_min:x_max:x_delta]

    # Evaluate the model
    evaluated_model = model(x_plotvalues, y_plotvalues)

    # Return the evaluated data
    return evaluated_model



def fit_polynomial(data, degree, mask=None):

    ySize,xSize = np.shape(data)

    # Fit the data using astropy.modeling
    poly_init = models.Polynomial2D(degree=degree)
    fit_model = fitting.LevMarLSQFitter()

    # Split x, y and z values that are not masked
    x_values, y_values, z_values = split_xyz(data, mask=mask, arrays=True)

    # Ignore model linearity warning from the fitter
    with warnings.catch_warnings():

        warnings.simplefilter('ignore')
        poly = fit_model(poly_init, x_values, y_values, z_values)  # What comes out is the model with the parameters set

    # Return the polynomial model
    sky = evaluate_model(poly, 0, xSize, 0, ySize)    
    
    return sky




def sky_subtraction(input_image, mask_image, polynomial_degree=5, output_image='sky_subtr.fits', output_sky=None, hdu_inp=0):    
      hdulist = pyfits.open(input_image)
      data = hdulist[hdu_inp].data      
      header = hdulist[hdu_inp].header
      ny,nx = np.shape(data)

      hdulist_mask = pyfits.open(mask_image)
      mask = hdulist_mask[0].data  
      
      # Create astropy mask (boolean)
      mask_astropy = convert_segm_to_boolean(mask)

      # Find average sky
      mean, median, std = sigma_clipped_stats(data, sigma=3.0, iters=5, mask=mask_astropy)
      
      # Fit the sky
      if polynomial_degree==0:
        sky = median
      else:
        sky = fit_polynomial(data, int(polynomial_degree), mask=mask_astropy)
      
      # Add keywords about sky
      header['Sky_med'] = median
      header['Sky_mean'] = mean
      header['Sky_std'] = std      
      
      # Save the output images
      outHDU = pyfits.PrimaryHDU(data-sky, header=header)
      outHDU.writeto(output_image, clobber=True)
      
      if output_sky is not None:
        outHDU = pyfits.PrimaryHDU(sky, header=header)
        outHDU.writeto(output_sky, clobber=True)
      return output_image

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Fitting sky background with a polynomial")
    parser.add_argument("inputImage", help="Input fits image with the centered object")
    parser.add_argument("inputMask", help="Input mask image",type=str) 
    parser.add_argument("--n", help="Optional: Polynomial degree (0 is constant level)", type=int, default=0) 
    parser.add_argument("--output_image", help="Optional: Output image", type=str, default=None) 
    parser.add_argument("--output_sky", help="Optional: Output sky", type=str, default=None)
    parser.add_argument("--hdu", help="Optional: HDU layer", type=int, default=0)
    
    args = parser.parse_args()

    input_image = args.inputImage
    mask_image = args.inputMask
    output_image = args.output_image
    output_sky = args.output_sky
    hdu = args.hdu
    n = args.n



    sky_subtraction(input_image, mask_image, polynomial_degree=n, output_image=output_image, output_sky=output_sky, hdu_inp=hdu)
