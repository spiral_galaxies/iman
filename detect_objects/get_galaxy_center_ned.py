import argparse
from astropy.io import fits as pyfits
from astropy import wcs
from astroquery import ned
import numpy as np


def main(input_image, name=None, RA=None, DEC=None):
    if name is None and Ra is None and DEC is None:
        print('ERROR! No object is specified!')
        exit()
    if name is not None:    
        result_table = ned.Ned.query_object(name)
        RA,DEC = float(result_table['RA(deg)']),float(result_table['DEC(deg)']) # an astropy.table.Table

    HDU = pyfits.open(input_image)
    header = HDU[0].header

    w = wcs.WCS(header)
    world = np.array([[RA,DEC]], np.float_)
    pixcrd = w.wcs_world2pix(world, 1)
    xc,yc = pixcrd[0][0],pixcrd[0][1]
    print(xc,yc)
    return xc,yc


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Sky background estimation")
    parser.add_argument("inputImage", help="Input fits image with the centered object")
    parser.add_argument("--name", help="Galaxy name (NED)",type=str,default=None) 
    parser.add_argument("--ra", nargs='?', const=1, help="RA (deg) if no name is specified.",type=float, default=None) 
    parser.add_argument("--dec", nargs='?', const=1, help="DEC (deg) if no name is specified.",type=float, default=None) 
    
    args = parser.parse_args()

    input_image = args.inputImage
    name = args.name
    RA = args.ra
    DEC = args.dec

    main(input_image, name=name, RA=RA, DEC=DEC)