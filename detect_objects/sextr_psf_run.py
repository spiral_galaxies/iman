# THE SCRIPT TO RUN SEXTRACTOR
#!/usr/bin/python
# -*- coding:  cp1251 -*-

import sys
import math
import numpy as np
from scipy import stats
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import matplotlib.path as path
from matplotlib.ticker import NullFormatter
from numpy import *
from pylab import *
import os
import shutil
import subprocess
import random
import pyfits

DECA_PATH = os.path.dirname(__file__)

sys.path.append(DECA_PATH)


import imp_setup

tmp_out = sys.stdout

def run_sextr(file_in,m0,GAIN,pix2sec,fwhm,output_cat):
	minarea = imp_setup.DETECT_MINAREA
	thresh = imp_setup.DETECT_THRESH
	filt_name = imp_setup.FILTER_NAME
	deb_nthr = imp_setup.DEBLEND_NTHRESH
	deb_minc = imp_setup.DEBLEND_MINCONT 
	cl_par = imp_setup.CLEAN_PARAM 
	ph_auto = str(imp_setup.PHOT_AUTOPARAMS[0]) + "," + str(imp_setup.PHOT_AUTOPARAMS[1])
	back_size = imp_setup.BACK_SIZE
	filt_size = imp_setup.BACK_FILTERSIZE
	back_type = imp_setup.BACKPHOTO_TYPE
	mem = imp_setup.MEMORY_PIXSTACK

	#FUNCTION TO RUN SEXTRACTOR WITH DEFAULT INITIAL PARAMETERS
	path1 = DECA_PATH + '/Sextractor_psf/default.conv'
	shutil.copy(path1,r"default.conv")
	path2 = DECA_PATH + '/Sextractor_psf/default.psf'
	shutil.copy(path2,r"default.psf")
	path3 = DECA_PATH + '/Sextractor_psf/default.sex'
	shutil.copy(path3,r"default.sex")
	path4 = DECA_PATH + '/Sextractor_psf/default.param'
	shutil.copy(path4,r"default.param")
	path5 = DECA_PATH + '/Sextractor_psf/default.nnw'
	shutil.copy(path5,r"default.nnw")
	pars = " -MAG_ZEROPOINT " + str(m0) + " -GAIN " + str(GAIN) + " -PIXEL_SCALE " + str(pix2sec) + " -SEEING_FWHM " + str(fwhm) + " -DETECT_MINAREA " + str(minarea) + " -DETECT_THRESH " + str(thresh) + " -FILTER_NAME " + str(filt_name) + " -DEBLEND_NTHRESH " + str(deb_nthr) + " -DEBLEND_MINCONT " + str(deb_minc) + " -CLEAN_PARAM " + str(cl_par) + " -PHOT_AUTOPARAMS " + str(ph_auto) + " -BACK_SIZE " + str(back_size) + " -BACK_FILTERSIZE " + str(filt_size) + " -BACKPHOTO_TYPE " + str(back_type) + " -MEMORY_PIXSTACK " + str(mem)

	# detect the name of SExtractor
	if subprocess.call("which sex >/dev/null", shell=True) == 0:
	    callString = "sex "
	elif subprocess.call("which sextractor >/dev/null", shell=True) == 0:
	    callString = "sextractor "
	else:
	    print "SExtractor was not found. Exiting..."
	    exit(1)

	s = callString + file_in + " -c" + " default.sex " + pars + " -VERBOSE_TYPE=QUIET"



	#print s
	subprocess.call(s, shell=True)
	shutil.move('field.cat',output_cat)
	#os.remove('aper.fits')
	os.remove('default.conv')
	os.remove('default.nnw')
	os.remove('default.param')
	os.remove('default.psf')
	os.remove('default.sex')
	os.remove('fon.fits')
	os.remove('objects.fits')





	