# THE SCRIPT TO RUN SEXTRACTOR
#!/usr/bin/python
# -*- coding:  cp1251 -*-

import sys
import math
import numpy as np
from scipy import stats
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import matplotlib.path as path
from matplotlib.ticker import NullFormatter
from numpy import *
from pylab import *
import os
import shutil
import subprocess
import random
import pyfits

DECA_PATH = os.path.dirname(__file__)

sys.path.append(DECA_PATH)



tmp_out = sys.stdout


import imp_setup


def GoodStars_find(catalogue_input,nx,ny):
	star_cl = imp_setup.star_cl
	number,x_image,y_image,x_world,y_world,XPEAK_IMAGE,YPEAK_IMAGE,XPEAK_WORLD,YPEAK_WORLD,flux_radius25,flux_radius50,flux_radius75,flux_radius99,mag_auto,a_image,b_image,theta_image,ellipticity,kron_radius,backgr,class_star,fwhm_gau,chi2_psf = loadtxt(catalogue_input, usecols=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], unpack=True, skiprows = 20)

	aver_mag = median(mag_auto)
	backgr_level_sum = median(backgr)
	A_good_r = 2.*median(a_image*kron_radius)
	A_good_l = 1.*median(a_image*kron_radius)
	xc_star = []; yc_star = []; fwhm_star = []; m_star = []; PA_star = []; ell_star = []; backgr_level = []; R = []; a_star = []; b_star = []; A_image =[]; Q_image = []
	Chi2_psf = []; Class_star = []

	good_chi2 = np.mean(chi2_psf) - 3.*np.std(chi2_psf)
	
	if isinstance(number, (list, tuple, np.ndarray)):
	  for k in range(0,len(number)):
			  #if class_star[k] > star_cl or chi2_psf[k] < good_chi2:
			  if a_image[k]*kron_radius[k]>=A_good_l and a_image[k]*kron_radius[k]<=A_good_r and b_image[k]/a_image[k]>=0.7 and b_image[k]/a_image[k]<=1 and x_image[k]> nx/10. and x_image[k]<0.9*nx and y_image[k]> ny/10. and y_image[k]<0.9*ny:
			      xc_star.append(x_image[k])
			      yc_star.append(y_image[k])
			      fwhm_star.append(fwhm_gau[k])
			      m_star.append(mag_auto[k])
			      PA_star.append(theta_image[k])
			      ell_star.append(ellipticity[k])
			      backgr_level.append(backgr[k])
			      R.append(kron_radius[k])
			      a_star.append(a_image[k]*kron_radius[k])
			      b_star.append(b_image[k]*kron_radius[k])
			      Chi2_psf.append(chi2_psf[k])
			      Class_star.append(class_star[k])
	  Nstars = len(fwhm_star)

	  xc_star = np.array(xc_star, dtype=float)
	  yc_star = np.array(yc_star, dtype=float)
	  fwhm_star = np.array(fwhm_star, dtype=float)
	  m_star = np.array(m_star, dtype=float)
	  PA_star = np.array(PA_star, dtype=float)
	  ell_star = np.array(ell_star, dtype=float)
	  backgr_level = np.array(backgr_level, dtype=float)
	  R = np.array(R, dtype=float)
	  a_star = np.array(a_star, dtype=float)
	  b_star = np.array(b_star, dtype=float)
	  Chi2_psf = np.array(Chi2_psf, dtype=float)
	  Class_star = np.array(Class_star, dtype=float)

	  print "Total number of good PSF stars is %i" % (Nstars)
	  import heapq
	  indices = heapq.nlargest(imp_setup.N_psf_stars, range(len(Class_star)), Class_star.take)

	  print "Only %i stars are selected" % (imp_setup.N_psf_stars)

	  return xc_star[indices],yc_star[indices],fwhm_star[indices],m_star[indices],PA_star[indices],ell_star[indices],backgr_level[indices],R[indices],a_star[indices],b_star[indices]
	else:
	  return x_image,y_image,fwhm_gau,mag_auto,theta_image,ellipticity,backgr,kron_radius,a_image*kron_radius,b_image*kron_radius

def main(catalogue_input,reg_output,nx,ny):
  fail = 1
  xc_star,yc_star,fwhm_star,m_star,PA_star,ell_star,backgr_level,R,a_star,b_star = GoodStars_find(catalogue_input,nx,ny)
  if isinstance(xc_star, (list, tuple, np.ndarray)):
    if len(xc_star)!=0:
      f_reg = open(reg_output, "w")
      print >>f_reg, '# Region file format: DS9 version 4.1'
      print >> f_reg, 'global color=green dashlist=8 3 width=1 font=\"helvetica 6 normal roman\" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1'
      print >>f_reg, 'image'
      for k in range(len(xc_star)):
	  print >>f_reg, 'ellipse(%f,%f,%f,%f,%f) # color=red width=2 text={%.3f,%.3f,%.5f}' % (xc_star[k],yc_star[k],a_star[k],b_star[k],PA_star[k],m_star[k],fwhm_star[k],backgr_level[k])
      f_reg.close()
      fail = 0
    else:
      open(reg_output, "a").close()
      fail = 1
    return fail
  else:
      f_reg = open(reg_output, "w")
      print >>f_reg, '# Region file format: DS9 version 4.1'
      print >> f_reg, 'global color=green dashlist=8 3 width=1 font=\"helvetica 6 normal roman\" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1'
      print >>f_reg, 'image'
      print >>f_reg, 'ellipse(%f,%f,%f,%f,%f) # color=red width=2 text={%.3f,%.3f,%.5f}' % (xc_star,yc_star,a_star,b_star,PA_star,m_star,fwhm_star,backgr_level)
      f_reg.close()
      return 0










	