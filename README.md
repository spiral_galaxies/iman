# README #

# /masking #

## auto_masking.py ##
*DESCRIPTION:*

Script to do auto masking of galaxy image using Sextractor.
Detected sources can be determined by changing snr and min_pix,
as well as using a different input sextractor file.
The target galaxy can be unmasked if needed.
The masked areas can be presented as ellipses or polygons.
The masked areas can be enlarged (by multiplication or subtraction).

**MINIMAL USAGE:**

`python auto_masking.py [input_image]`

## inner_masking.py ##
*DESCRIPTION:*

Script to mask stars inside of a galaxy image using a given PSF image.
The target galaxy can be unmasked if needed.
The masked areas can be presented as ellipses or polygons.
The masked areas can be enlarged (by multiplication or subtraction).

**MINIMAL USAGE:**

`python inner_masking.py [input_image] [psf_image]`

##merge_masks.py##
*DESCRIPTION:*

Script to merge several masks into one.
Both region and fits formats are supported. Input files should be separated by comma.

**MINIMAL USAGE:**

`python merge_masks.py [input_masks_separated_by_comma] [output_mask]`

##convert_reg_to_mask.py##
*DESCRIPTION:*

Script to convert region mask to fits mask.
Pixels with counts >0 are a mask in the output mask image.

**MINIMAL USAGE:**

`python convert_reg_to_mask.py [input_image] [region_file]`

##convert_segm_to_region.py##
*DESCRIPTION:*

Script to convert a segmentation map from Sextractor or PTS to the DS9 region file consisting of polygons.

**MINIMAL USAGE:**

`python convert_segm_to_region.py [segm_file] [output_region_file]`




#/sky_fitting#

## determine_sky.py ##
*DESCRIPTION:*

Script fit sky background with a polynomial of n degree. A mask is required.
**MINIMAL USAGE:**

`python determine_sky.py [input_image] [input_mask]`

##sky_around_galaxy.py##
*DESCRIPTION:*

A script to fit sky background in an annulus around galaxy. An outer galaxy ellipse --galaxy_ellipse (where outermost isophotes end) is required, either as a DS9 region file (ellipse region in images coordinates), or in the format xc,yc,sma,smb,PA. The width of the annulus is controlled by the key --annulus_width (in pix). To check the annulus in DS9 before starting the fitting the key --manual should be given. For now, only constant background level (--degree 0) within the annulus is computed.

**MINIMAL USAGE:**

`python sky_around_galaxy.py [input_image] --galaxy_ellipse [region_file OR xc,yc,sma,smb,PA]`
